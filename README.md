# Laravel Tips

Awesome Laravel tips and tricks for all artisans. PR and ideas are welcome!

An idea by [PovilasKorop](https://github.com/PovilasKorop) and [MarceauKa](https://github.com/MarceauKa).

Hey, like these tips? Also, check out my premium [Laravel courses](https://laraveldaily.teachable.com/).

---

Or if you want the Chinese version:
[中文版本](https://github.com/Lysice/laravel-tips-chinese/blob/master/README-zh.md)

---

__Update 03 April 2022__: Currently there are __289 tips__ divided into 14 sections.

## Table of Contents

- [DB Models and Eloquent](DB_Models_and_Eloquent.md) (82 tips) [0/82] переведено
- [Models Relations](Models_Relations.md) (35 tips) [0/35] переведено
- [Migrations](Migrations.md) (13 tips) [0/13] переведено
- [Views](Views.md) (15 tips) [0/15] переведено
- [Routing](Routing.md) (26 tips) [0/26] переведено
- [Validation](Validation.md) (20 tips) [0/20] переведено
- [Collections](Collections.md) (8 tips) [0/8] переведено
- [Auth](Auth.md) (5 tips) [0/5] переведено
- [Mail](Mail.md) (6 tips) [0/6] переведено
- [Artisan](Artisan.md) (7 tips) [0/7] переведено
- [Factories](Factories.md) (6 tips) [0/6] переведено
- [Log and debug](Log_and_Debug.md) (5 tips) [0/5] переведено
- [API](Api.md) (5 tips) [5/5] переведено
- [Other](Other.md) (56 tips) [0/56] переведено
