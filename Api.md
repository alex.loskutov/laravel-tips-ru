## API

⬆️ [Вернутся в главное меню](README.md#laravel-tips) ⬅️ [Предыдущее (Log and Debug)](Log_and_Debug.md) ➡️ [Следующее (Прочее)](Other.md)

- [Ресурсы API (Resources): С или Без "data"](#api-возвращаем-всё-прошло-нормально)
- [API Возвращает "Всё прошло нормально"](#api-возвращаем-всё-прошло-нормально)
- [Избегайте N+1 запросов к API ресурсам](#избегайте-n1-запросов-к-api-ресурсам)
- [Получение Bearer Токена из заголовка Authorization](#получение-bearer-токена-из-заголовка-authorization)
- [Сортировка результатов вашего API](#сортировка-результатов-вашего-api)

### Ресурсы API (Resources): С или Без "data"

Если вы используете Ресурсы API Eloquent для возвращения данных, они автоматически оборачиваются в 'data'. Если вы хотите отключить обёртывание данных, добавьте `JsonResource::withoutWrapping();` в `app/Providers/AppServiceProvider.php`.

```php
class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        JsonResource::withoutWrapping();
    }
}
```

Совет дал [@phillipmwaniki](https://twitter.com/phillipmwaniki/status/1445230637544321029)

### API возвращаем "Всё прошло нормально"

Если у вас есть конечная точка API, которая выполняет какие-то действия но не возвращает ответ, и вы просто хотите вернуть "Всё прошло нормально". Вы можете вернуть код состояния 204 "Нет данных". В Laravel это очень просто: `return response()->noContent();`.

```php
public function reorder(Request $request)
{
    foreach ($request->input('rows', []) as $row) {
        Country::find($row['id'])->update(['position' => $row['position']]);
    }

    return response()->noContent();
}
```

### Избегайте N+1 запросов к API ресурсам

Вы можете избежать N+1 запросы к API ресурсам используя метод `whenLoaded()`.

В приведённом ниже примере, `department` будет добавлен только в том случае, если он уже загружен в модель `Employee`. Без `whenLoaded()` всегда будет добавляться запрос для `department`.

```php
class EmployeeResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->uuid,
            'fullName' => $this->full_name,
            'email' => $this->email,
            'jobTitle' => $this->job_title,
            'department' => DepartmentResource::make($this->whenLoaded('department')),
        ];
    }
}
```

Совет дал [@mmartin_joo](https://twitter.com/mmartin_joo/status/1473987501501071362)

### Получение Bearer Токена из заголовка Authorization

Функция `bearerToken()` очень удобна, когда вы работаете с API и хотите получить токен доступа из заголовка Authorization.

```php
// Не разбирайте заголовок вручную, как это сделано здесь:
$tokenWithBearer = $request->header('Authorization');
$token = substr($tokenWithBearer, 7);

//Делайте так:
$token = $request->bearerToken();
```

Совет дал [@iamharis010](https://twitter.com/iamharis010/status/1488413755826327553)

### Сортировка результатов вашего API

Сортировка API по одному столбцу с контролем направления

```php
// Обрабатываем /dogs?sort=name и /dogs?sort=-name
Route::get('dogs', function (Request $request) {
    // Получаем параметр запроса сортировки (или возвращаемся к сортировке по умолчанию "name")
    $sortColumn = $request->input('sort', 'name');

    // Устанавливает направление сортировки в зависимости от того,
    // начинается ли ключ с "-", используя хэлпер Str::startsWith()
    $sortDirection = Str::startsWith($sortColumn, '-') ? 'desc' : 'asc';
    $sortColumn = ltrim($sortColumn, '-');

    return Dog::orderBy($sortColumn, $sortDirection)
        ->paginate(20);
});
```

Может выполнятся для нескольких столбцов (например, ?sort=name,-weight)

```php
// Обрабатываем ?sort=name,-weight
Route::get('dogs', function (Request $request) {
    // Берём параметр запроса и превращает в массив используя "," как разделитель
    $sorts = explode(',', $request->input('sort', ''));

    // Создаём запрос
    $query = Dog::query();

    // Добавляем сортировки, одну за другой
    foreach ($sorts as $sortColumn) {
        $sortDirection = Str::startsWith($sortColumn, '-') ? 'desc' : 'asc';
        $sortColumn = ltrim($sortColumn, '-');

        $query->orderBy($sortColumn, $sortDirection);
    }

    // Возвращаем
    return $query->paginate(20);
});
```
